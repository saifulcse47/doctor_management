<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutdoorPatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outdoor_patients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('registration_no');
            $table->string('name');
            $table->string('age');
            $table->string('phone');
            $table->tinyInteger('gender');
            $table->tinyInteger('patient_status');
            $table->tinyInteger('pregnant');
            $table->tinyInteger('peptic_ulcer');
            $table->tinyInteger('fatty_liver');
            $table->tinyInteger('sol_liver');
            $table->tinyInteger('acute_hepaties');
            $table->tinyInteger('obstructive_jaundice');
            $table->tinyInteger('liver_abscess');
            $table->tinyInteger('liver_cyst');
            $table->string('comment');
            $table->tinyInteger('agree');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outdoor_patients');
    }
}
