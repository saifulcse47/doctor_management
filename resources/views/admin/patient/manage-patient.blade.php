@extends('admin.dashboard')
@section('contents')
    <!-- Page Header -->
    <div class="page-header">
        <div class="row">
            <div class="col">
                <h3 class="page-title">Outdoor Patient</h3>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                    <li class="breadcrumb-item active">Outdoor Patient</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /Page Header -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Outdoor Patient</h4>
                </div>
                @if(session('message'))
                    <div class="alert alert-success" style="background-color: green;color: white" role="alert">
                        {{Session::get('message')}}
                    </div>
                @endif
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="datatable table table-stripped">
                            <thead>
                            <tr>
                                <th>Reg.No</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Patient Status</th>
                                <th>Age</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($patients as $data)
                                <tr>
                                    <td>{{$data->registration_no}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->phone}}</td>
                                    @if($data->patient_status==0)
                                        <td>old</td>
                                    @else
                                        <td>old</td>
                                    @endif
                                    <td>{{$data->age}}</td>
                                    <td>{{$data->created_at}}</td>
                                    <td><a href="" class="btn btn-warning btn-sm">Edit</a>
                                        <a href="{{route('view.patient',['id'=>$data->id])}}" class="btn btn-success btn-sm">View</a>
                                        <a href="" class="btn btn-danger btn-sm">Delete</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
