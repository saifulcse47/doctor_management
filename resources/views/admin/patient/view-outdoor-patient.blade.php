@extends('admin.dashboard')
@section('contents')
    <!-- Page Header -->
    <div class="page-header">
        <div class="row">
            <div class="col">
                <h3 class="page-title">Profile</h3>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                    <li class="breadcrumb-item active">Profile</li>
                </ul>
                @if($errors->has('agree'))
                <div class="alert alert-danger" role="alert">
                    <span class="text-danger">Need click checkbox agree otherwise can not update data</span>
                </div>
                @endif
            </div>
        </div>
    </div>
    <!-- /Page Header -->

    <div class="row">
        <div class="col-md-12">
            <div class="profile-header">
                <div class="row align-items-center">
                    <div class="col-auto profile-image">
                        <a href="#">
                            <img class="rounded-circle" alt="User Image" src="{{asset('admin/assets/img/profiles/avatar-01.jpg')}}">
                        </a>
                    </div>
                    <div class="col ml-md-n2 profile-user-info">
                        <h4 class="user-name mb-0">{{$patient->name}}</h4>
                        <h6 class="text-muted">Phone: {{$patient->phone}}</h6>
                    </div>
                    <div class="col-auto profile-btn">

                        <a href="#" class="btn btn-primary">
                            Edit
                        </a>
                    </div>
                </div>
            </div>
            <div class="profile-menu">
                <ul class="nav nav-tabs nav-tabs-solid">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#per_details_tab">About</a>
                    </li>
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" data-toggle="tab" href="#password_tab">Password</a>--}}
{{--                    </li>--}}
                </ul>
            </div>
            <div class="tab-content profile-tab-cont">

                <!-- Personal Details Tab -->
                <div class="tab-pane fade show active" id="per_details_tab">

                    <!-- Personal Details -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title d-flex justify-content-between border-bottom">
                                        <span>Patient Details</span>
                                        <a class="edit-link" data-toggle="modal" href="#editPatient-{{ $patient->id }}"><i class="fa fa-edit mr-1"></i>Edit</a>
                                    </h5>
                                    <div class="row border-bottom">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">Reg. No:</p>
                                        <p class="col-sm-10">{{$patient->registration_no}}</p>
                                    </div>
                                    <div class="row border-bottom">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">Age:</p>
                                        <p class="col-sm-10">{{$patient->age}}</p>
                                    </div>
                                    <div class="row border-bottom">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0">Gender:</p>
                                        @if($patient->gender==0)
                                            <p class="col-sm-10">Male</p>
                                            @else
                                            <p class="col-sm-10">Female</p>
                                            @endif
                                    </div>
                                    <div class="row border-bottom">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0">Patient Status:</p>
                                        @if($patient->patient_status==0)
                                            <p class="col-sm-10">Old Patient</p>
                                        @else
                                            <p class="col-sm-10">New Patient</p>
                                        @endif
                                    </div>
                                    <div class="row border-bottom">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0">Pregnant:</p>
                                        @if($patient->pregnant==0)
                                            <p class="col-sm-10">Yes</p>
                                        @else
                                            <p class="col-sm-10">No</p>
                                        @endif
                                    </div>
                                    <div class="row border-bottom">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0">Peptic ulcer:</p>
                                        @if($patient->peptic_ulcer==0)
                                            <p class="col-sm-10">Yes</p>
                                        @elseif($patient->peptic_ulcer==1)
                                            <p class="col-sm-10">No</p>
                                        @else
                                            <p class="col-sm-10">Other</p>
                                        @endif
                                    </div>
                                    <div class="row border-bottom">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0">Fatty Liver:</p>
                                        @if($patient->fatty_liver==0)
                                            <p class="col-sm-10">Yes</p>
                                        @else
                                            <p class="col-sm-10">No</p>
                                        @endif
                                    </div>
                                    <div class="row border-bottom">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0">SOL Liver:</p>
                                        @if($patient->sol_liver==0)
                                            <p class="col-sm-10">HCC</p>
                                        @elseif($patient->sol_liver==1)
                                            <p class="col-sm-10">Secondary</p>
                                        @elseif($patient->sol_liver==2)
                                            <p class="col-sm-10">Unknown</p>
                                        @endif
                                    </div>
                                    <div class="row border-bottom">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0">Acute Hepaties:</p>
                                        @if($patient->acute_hepaties==0)
                                            <p class="col-sm-10">A</p>
                                        @elseif($patient->acute_hepaties==1)
                                            <p class="col-sm-10">B</p>
                                        @elseif($patient->acute_hepaties==2)
                                            <p class="col-sm-10">C</p>
                                        @elseif($patient->acute_hepaties==3)
                                            <p class="col-sm-10">E</p>
                                        @elseif($patient->acute_hepaties==4)
                                            <p class="col-sm-10">Drugs</p>
                                        @elseif($patient->acute_hepaties==5)
                                            <p class="col-sm-10">Unspecified</p>
                                        @elseif($patient->acute_hepaties==6)
                                            <p class="col-sm-10">Other</p>
                                        @endif
                                    </div>
                                    <div class="row border-bottom">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0">Obstructive Jaundice:</p>
                                        @if($patient->obstructive_jaundice==0)
                                            <p class="col-sm-10">Yes</p>
                                        @elseif($patient->obstructive_jaundice==1)
                                            <p class="col-sm-10">No</p>
                                        @else
                                            <p class="col-sm-10">Other</p>
                                        @endif
                                    </div>
                                    <div class="row border-bottom">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0">Liver Abscess:</p>
                                        @if($patient->liver_abscess==0)
                                            <p class="col-sm-10">Yes</p>
                                        @elseif($patient->liver_abscess==1)
                                            <p class="col-sm-10">No</p>
                                        @else
                                            <p class="col-sm-10">Unknown</p>
                                        @endif
                                    </div>
                                    <div class="row border-bottom">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0">Liver Cyst:</p>
                                        @if($patient->liver_cyst==0)
                                            <p class="col-sm-10">Simple</p>
                                        @elseif($patient->liver_cyst==1)
                                            <p class="col-sm-10">Hydatid</p>
                                        @else
                                            <p class="col-sm-10">Unknown</p>
                                        @endif
                                    </div>
                                    <div class="row border-bottom">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">Additional Comment:</p>
                                        <p class="col-sm-10">{{$patient->comment}}</p>
                                    </div>
                                </div>
                            </div>

                            <!-- Edit Details Modal -->
                            <div class="modal fade" id="editPatient-{{ $patient->id }}" aria-hidden="true" role="dialog">
                                <div class="modal-dialog modal-dialog-centered" role="document" >
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Personal Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('update.out-door-patient')}}" method="POST">
                                                @csrf
                                                <div class="form-group row">
                                                    <label for="registration_no" class="col-sm-2 col-form-label">Registration No.</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="registration_no" value="{{$patient->registration_no}}" class="form-control"  placeholder="Registration No.">
{{--                                                        <input type="hidden" required name="registration_no" class="form-control"  placeholder="Registration No.">--}}
                                                        <input type="hidden" name="id" value="{{ $patient->id }}" class="form-control" placeholder="id">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-2 col-form-label">Patient Name</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" required name="name" value="{{$patient->name}}" class="form-control"  placeholder="Patient Name">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="age" class="col-sm-2 col-form-label">Age.</label>
                                                    <div class="col-sm-10">
                                                        <input type="number" required name="age" value="{{$patient->age}}" class="form-control"  placeholder="Patient Name age">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="phone" class="col-sm-2 col-form-label">Phone Number</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" required name="phone" value="{{$patient->phone}}" class="form-control"  placeholder="Phone Number">
                                                    </div>
                                                </div>
                                                <fieldset class="form-group">
                                                    <div class="row">
                                                        <legend class="col-form-label col-sm-2 pt-0">Gender</legend>
                                                        <div class="col-sm-10 border-bottom mb-3">
                                                            <div class="form-check">
                                                                <input class="form-check-input" {{$patient ->gender==0 ? 'checked': ''}} type="radio" name="gender" id="gridRadios1" value="0" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Male
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="gender" {{$patient ->gender==1 ? 'checked': ''}} id="gridRadios1" value="1" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Female
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <legend class="col-form-label col-sm-2 pt-0">Patient Status</legend>
                                                        <div class="col-sm-10 border-bottom mb-3">
                                                            <div class="form-check">
                                                                <input class="form-check-input" {{$patient ->patient_status==0 ? 'checked': ''}} type="radio" name="patient_status" id="gridRadios1" value="0" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Old Patient
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" {{$patient ->patient_status==1 ? 'checked': ''}} type="radio" name="patient_status" id="gridRadios1" value="1" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    New Patient
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <legend class="col-form-label col-sm-2 pt-0">Pregnant</legend>
                                                        <div class="col-sm-10 border-bottom mb-3">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="pregnant" {{$patient ->pregnant==0 ? 'checked': ''}} id="gridRadios1" value="0" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Yes
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="pregnant" {{$patient ->pregnant==1 ? 'checked': ''}} id="gridRadios1" value="1" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    No
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <legend class="col-form-label col-sm-2 pt-0">Peptic Ulcer?</legend>
                                                        <div class="col-sm-10 border-bottom mb-3">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="peptic_ulcer" {{$patient ->peptic_ulcer==0 ? 'checked': ''}} id="gridRadios1" value="0" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Yes
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="peptic_ulcer" {{$patient ->peptic_ulcer==1 ? 'checked': ''}} id="gridRadios1" value="1" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    No
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="peptic_ulcer" {{$patient ->peptic_ulcer==2 ? 'checked': ''}} id="gridRadios1" value="2" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Other
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <legend class="col-form-label col-sm-2 pt-0">Fatty Liver</legend>
                                                        <div class="col-sm-10 border-bottom mb-3">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="fatty_liver" {{$patient ->fatty_liver==0 ? 'checked': ''}} id="gridRadios1" value="0" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Yes
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="fatty_liver" {{$patient ->fatty_liver==1 ? 'checked': ''}} id="gridRadios1" value="1" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    No
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <legend class="col-form-label col-sm-2 pt-0">SOL Liver</legend>
                                                        <div class="col-sm-10 border-bottom mb-3">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" {{$patient ->sol_liver==0 ? 'checked': ''}} name="sol_liver" id="gridRadios1" value="0" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    HCC
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" {{$patient ->sol_liver==1 ? 'checked': ''}} type="radio" name="sol_liver" id="gridRadios1" value="1" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Secondary
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" {{$patient ->sol_liver==2 ? 'checked': ''}} type="radio" name="sol_liver" id="gridRadios1" value="2" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Unknown
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <legend class="col-form-label col-sm-2 pt-0">Acute Hepaties</legend>
                                                        <div class="col-sm-10 border-bottom mb-3">
                                                            <div class="form-check">
                                                                <input class="form-check-input" {{$patient ->acute_hepaties==0 ? 'checked': ''}} type="radio" name="acute_hepaties" id="gridRadios1" value="0" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    A
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="acute_hepaties" {{$patient ->acute_hepaties==1 ? 'checked': ''}} id="gridRadios1" value="1" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    B
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="acute_hepaties" {{$patient ->acute_hepaties==2 ? 'checked': ''}} id="gridRadios1" value="2" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    C
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="acute_hepaties" {{$patient ->acute_hepaties==3 ? 'checked': ''}} id="gridRadios1" value="3" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    E
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="acute_hepaties" {{$patient ->acute_hepaties==4 ? 'checked': ''}} id="gridRadios1" value="4" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Drugs
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="acute_hepaties" {{$patient ->acute_hepaties==5 ? 'checked': ''}} id="gridRadios1" value="5" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Unspecified
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="acute_hepaties" id="gridRadios1" {{$patient ->acute_hepaties==6 ? 'checked': ''}} value="6" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Other
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <legend class="col-form-label col-sm-2 pt-0">Obstructive Jaundice</legend>
                                                        <div class="col-sm-10 border-bottom mb-3">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="obstructive_jaundice" {{$patient ->obstructive_jaundice==0 ? 'checked': ''}} id="gridRadios1" value="0" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Yes
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="obstructive_jaundice" {{$patient ->obstructive_jaundice==1 ? 'checked': ''}} id="gridRadios1" value="1" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    No
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="obstructive_jaundice" id="gridRadios1" {{$patient ->obstructive_jaundice==2 ? 'checked': ''}} value="2" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Other
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <legend class="col-form-label col-sm-2 pt-0">Liver Abscess</legend>
                                                        <div class="col-sm-10 border-bottom mb-3">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="liver_abscess" id="gridRadios1" {{$patient ->liver_abscess==0 ? 'checked': ''}} value="0" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Yes
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" {{$patient ->liver_abscess==1 ? 'checked': ''}} name="liver_abscess" id="gridRadios1" value="1" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    No
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" {{$patient ->liver_abscess==2 ? 'checked': ''}} name="liver_abscess" id="gridRadios1" value="2" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Unknown
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <legend class="col-form-label col-sm-2 pt-0">Liver Cyst</legend>
                                                        <div class="col-sm-10 border-bottom mb-3">
                                                            <div class="form-check">
                                                                <input class="form-check-input" {{$patient ->liver_cyst==0 ? 'checked': ''}} type="radio" name="liver_cyst" id="gridRadios1" value="0" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Simple
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio"{{$patient ->liver_cyst==1 ? 'checked': ''}} name="liver_cyst" id="gridRadios1" value="1" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Hydatid
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="liver_cyst" {{$patient ->liver_cyst==2 ? 'checked': ''}} id="gridRadios1" value="2" checked>
                                                                <label class="form-check-label" for="gridRadios1">
                                                                    Unknown
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-2 col-form-label">Any additional Comment</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="comment" value="{{$patient->comment}}" class="form-control"  placeholder="Comment">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-2">I Agree with all additional data</div>
                                                    <div class="col-sm-10">
                                                        <div class="form-check">
                                                            <input class="form-check-input" name="agree" value="1" type="checkbox" id="gridCheck1">
                                                            <label class="form-check-label" for="gridCheck1">
                                                                Agree
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-10">
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Edit Details Modal -->

                        </div>


                    </div>
                    <!-- /Personal Details -->

                </div>
                <!-- /Personal Details Tab -->

                <!-- Change Password Tab -->
                <div id="password_tab" class="tab-pane fade">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Change Password</h5>
                            <div class="row">
                                <div class="col-md-10 col-lg-6">
                                    <form>
                                        <div class="form-group">
                                            <label>Old Password</label>
                                            <input type="password" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>New Password</label>
                                            <input type="password" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <input type="password" class="form-control">
                                        </div>
                                        <button class="btn btn-primary" type="submit">Save Changes</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Change Password Tab -->
            </div>
        </div>
    </div>
    <!-- /Page Wrapper -->
@endsection
