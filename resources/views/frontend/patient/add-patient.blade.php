<!DOCTYPE html>
<html lang="en">

<!-- doccure/  30 Nov 2019 04:11:34 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Doccure</title>

    <!-- Favicons -->
    <link type="image/x-icon" href="{{asset('frontend/assets/img/favicon.png')}}" rel="icon">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('frontend/assets/css/bootstrap.min.css')}}">

    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{asset('frontend/assets/plugins/fontawesome/css/fontawesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/assets/plugins/fontawesome/css/all.min.css')}}">

    <!-- Main CSS -->
    <link rel="stylesheet" href="{{asset('frontend/assets/css/style.css')}}">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="{{asset('frontend/assets/js/html5shiv.min.js')}}"></script>
    <script src="{{asset('frontend/assets/js/respond.min.js')}}"></script>
    <![endif]-->

</head>
<body class="account-page">

<!-- Main Wrapper -->
<div class="main-wrapper">

    <!-- Header -->
    <header class="header">
        <nav class="navbar navbar-expand-lg header-nav">
            <div class="navbar-header">
                <a id="mobile_btn" href="javascript:void(0);">
							<span class="bar-icon">
								<span></span>
								<span></span>
								<span></span>
							</span>
                </a>
                <a href="{{url('/')}}" class="navbar-brand logo">
                    <img src="{{asset('frontend/assets/img/logo.png')}}" class="img-fluid" alt="Logo">
                </a>
            </div>
            <div class="main-menu-wrapper">
                <div class="menu-header">
                    <a href="index-2.html" class="menu-logo">
                        <img src="{{asset('frontend/assets/img/logo.png')}}" class="img-fluid" alt="Logo">
                    </a>
                    <a id="menu_close" class="menu-close" href="javascript:void(0);">
                        <i class="fas fa-times"></i>
                    </a>
                </div>
                <ul class="main-nav">
                    <li class="active">
                        <a href="{{url('/')}}">Home</a>
                    </li>
                    {{--                <li class="has-submenu">--}}
                    {{--                    <a href="#">Doctors <i class="fas fa-chevron-down"></i></a>--}}
                    {{--                    <ul class="submenu">--}}
                    {{--                        <li><a href="doctor-dashboard.html">Doctor Dashboard</a></li>--}}
                    {{--                        <li><a href="appointments.html">Appointments</a></li>--}}
                    {{--                        <li><a href="schedule-timings.html">Schedule Timing</a></li>--}}
                    {{--                        <li><a href="my-patients.html">Patients List</a></li>--}}
                    {{--                        <li><a href="patient-profile.html">Patients Profile</a></li>--}}
                    {{--                        <li><a href="chat-doctor.html">Chat</a></li>--}}
                    {{--                        <li><a href="invoices.html">Invoices</a></li>--}}
                    {{--                        <li><a href="doctor-profile-settings.html">Profile Settings</a></li>--}}
                    {{--                        <li><a href="reviews.html">Reviews</a></li>--}}
                    {{--                        <li><a href="doctor-register.html">Doctor Register</a></li>--}}
                    {{--                    </ul>--}}
                    {{--                </li>--}}
                    <li class="has-submenu">
                        <a href="#">Patients <i class="fas fa-chevron-down"></i></a>
                        <ul class="submenu">
                            <li><a href="{{route('registration.patient')}}">Patients Registration</a></li>
                            {{--                        <li><a href="doctor-profile.html">Doctor Profile</a></li>--}}
                            {{--                        <li><a href="booking.html">Booking</a></li>--}}
                            {{--                        <li><a href="checkout.html">Checkout</a></li>--}}
                            {{--                        <li><a href="booking-success.html">Booking Success</a></li>--}}
                            {{--                        <li><a href="patient-dashboard.html">Patient Dashboard</a></li>--}}
                            {{--                        <li><a href="favourites.html">Favourites</a></li>--}}
                            {{--                        <li><a href="chat.html">Chat</a></li>--}}
                            {{--                        <li><a href="profile-settings.html">Profile Settings</a></li>--}}
                            {{--                        <li><a href="change-password.html">Change Password</a></li>--}}
                        </ul>
                    </li>
                    {{--                <li class="has-submenu">--}}
                    {{--                    <a href="#">Pages <i class="fas fa-chevron-down"></i></a>--}}
                    {{--                    <ul class="submenu">--}}
                    {{--                        <li><a href="voice-call.html">Voice Call</a></li>--}}
                    {{--                        <li><a href="video-call.html">Video Call</a></li>--}}
                    {{--                        <li><a href="search.html">Search Doctors</a></li>--}}
                    {{--                        <li><a href="calendar.html">Calendar</a></li>--}}
                    {{--                        <li><a href="components.html">Components</a></li>--}}
                    {{--                        <li class="has-submenu">--}}
                    {{--                            <a href="invoices.html">Invoices</a>--}}
                    {{--                            <ul class="submenu">--}}
                    {{--                                <li><a href="invoices.html">Invoices</a></li>--}}
                    {{--                                <li><a href="invoice-view.html">Invoice View</a></li>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                        <li><a href="blank-page.html">Starter Page</a></li>--}}
                    {{--                        <li><a href="login.html">Login</a></li>--}}
                    {{--                        <li><a href="register.html">Register</a></li>--}}
                    {{--                        <li><a href="forgot-password.html">Forgot Password</a></li>--}}
                    {{--                    </ul>--}}
                    {{--                </li>--}}
{{--                                    <li>--}}
{{--                                        <a href="{{route('admin.dashboard')}}" target="_blank">Admin</a>--}}
{{--                                    </li>--}}
                    <li class="login-link">
                        <a href="login.html">Login / Signup</a>
                    </li>
                </ul>
            </div>
            <ul class="nav header-navbar-rht">
                <li class="nav-item contact-item">
                    <div class="header-contact-img">
                        <i class="far fa-hospital"></i>
                    </div>
                    <div class="header-contact-detail">
                        <p class="contact-header">Contact</p>
                        <p class="contact-info-header"> +1 315 369 5943</p>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link header-login" href="login.html">login / Signup </a>
                </li>
            </ul>
        </nav>
    </header>

    <!-- /Header -->

    <!-- Page Content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-8 offset-md-2">
                    @if(session('message'))
                    <div class="alert alert-success" role="alert">
                        {{Session::get('message')}}
                    </div>
                    @endif
                    <h3>Outdoor Patient Liver Dept.</h3>
                    <!-- Register Content -->
                    <h4>Patient Data</h4>
                    <form action="{{route('save.out-door-patient')}}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label for="registration_no" class="col-sm-2 col-form-label">Registration No.</label>
                            <div class="col-sm-10">
                                <input type="number" required name="registration_no" class="form-control"  placeholder="Registration No.">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Patient Name</label>
                            <div class="col-sm-10">
                                <input type="text" required name="name" class="form-control"  placeholder="Patient Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="age" class="col-sm-2 col-form-label">Age.</label>
                            <div class="col-sm-10">
                                <input type="number" required name="age" class="form-control"  placeholder="Patient Name age">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phone" class="col-sm-2 col-form-label">Phone Number</label>
                            <div class="col-sm-10">
                                <input type="text" required name="phone" class="form-control"  placeholder="Phone Number">
                            </div>
                        </div>
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-2 pt-0">Gender</legend>
                                <div class="col-sm-10 border-bottom mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="gender" id="gridRadios1" value="0" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Male
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="gender" id="gridRadios1" value="1" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Female
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <legend class="col-form-label col-sm-2 pt-0">Patient Status</legend>
                                <div class="col-sm-10 border-bottom mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="patient_status" id="gridRadios1" value="0" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Old Patient
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="patient_status" id="gridRadios1" value="1" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            New Patient
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <legend class="col-form-label col-sm-2 pt-0">Pregnant</legend>
                                <div class="col-sm-10 border-bottom mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="pregnant" id="gridRadios1" value="0" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="pregnant" id="gridRadios1" value="1" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <legend class="col-form-label col-sm-2 pt-0">Peptic Ulcer?</legend>
                                <div class="col-sm-10 border-bottom mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="peptic_ulcer" id="gridRadios1" value="0" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="peptic_ulcer" id="gridRadios1" value="1" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            No
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="peptic_ulcer" id="gridRadios1" value="2" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Other
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <legend class="col-form-label col-sm-2 pt-0">Fatty Liver</legend>
                                <div class="col-sm-10 border-bottom mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="fatty_liver" id="gridRadios1" value="0" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="fatty_liver" id="gridRadios1" value="1" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <legend class="col-form-label col-sm-2 pt-0">SOL Liver</legend>
                                <div class="col-sm-10 border-bottom mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="sol_liver" id="gridRadios1" value="0" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            HCC
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="sol_liver" id="gridRadios1" value="1" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Secondary
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="sol_liver" id="gridRadios1" value="2" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Unknown
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <legend class="col-form-label col-sm-2 pt-0">Acute Hepaties</legend>
                                <div class="col-sm-10 border-bottom mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="acute_hepaties" id="gridRadios1" value="0" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            A
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="acute_hepaties" id="gridRadios1" value="1" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            B
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="acute_hepaties" id="gridRadios1" value="2" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            C
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="acute_hepaties" id="gridRadios1" value="3" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            E
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="acute_hepaties" id="gridRadios1" value="4" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Drugs
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="acute_hepaties" id="gridRadios1" value="5" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Unspecified
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="acute_hepaties" id="gridRadios1" value="6" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Other
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <legend class="col-form-label col-sm-2 pt-0">Obstructive Jaundice</legend>
                                <div class="col-sm-10 border-bottom mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="obstructive_jaundice" id="gridRadios1" value="0" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="obstructive_jaundice" id="gridRadios1" value="1" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            No
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="obstructive_jaundice" id="gridRadios1" value="2" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Other
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <legend class="col-form-label col-sm-2 pt-0">Liver Abscess</legend>
                                <div class="col-sm-10 border-bottom mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="liver_abscess" id="gridRadios1" value="0" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="liver_abscess" id="gridRadios1" value="1" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            No
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="liver_abscess" id="gridRadios1" value="2" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Unknown
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <legend class="col-form-label col-sm-2 pt-0">Liver Cyst</legend>
                                <div class="col-sm-10 border-bottom mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="liver_cyst" id="gridRadios1" value="0" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Simple
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="liver_cyst" id="gridRadios1" value="1" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Hydatid
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="liver_cyst" id="gridRadios1" value="2" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Unknown
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Any additional Comment</label>
                            <div class="col-sm-10">
                                <input type="text" name="comment" class="form-control"  placeholder="Comment">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2">I Agree with all additional data</div>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input" name="agree" value="1" type="checkbox" id="gridCheck1">
                                    <label class="form-check-label" for="gridCheck1">
                                        Agree
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                    <!-- /Register Content -->

                </div>
            </div>

        </div>

    </div>
    <!-- /Page Content -->

    <!-- Footer -->
    <footer class="footer">

        <!-- Footer Top -->
        <div class="footer-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-md-6">

                        <!-- Footer Widget -->
                        <div class="footer-widget footer-about">
                            <div class="footer-logo">
                                <img src="{{asset('frontend/assets/img/footer-logo.png')}}" alt="logo">
                            </div>
                            <div class="footer-about-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                <div class="social-icon">
                                    <ul>
                                        <li>
                                            <a href="#" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank"><i class="fab fa-twitter"></i> </a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank"><i class="fab fa-instagram"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank"><i class="fab fa-dribbble"></i> </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /Footer Widget -->

                    </div>

                    <div class="col-lg-3 col-md-6">

                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">For Patients</h2>
                            <ul>
                                <li><a href="search.html"><i class="fas fa-angle-double-right"></i> Search for Doctors</a></li>
                                <li><a href="login.html"><i class="fas fa-angle-double-right"></i> Login</a></li>
                                <li><a href="register.html"><i class="fas fa-angle-double-right"></i> Register</a></li>
                                <li><a href="booking.html"><i class="fas fa-angle-double-right"></i> Booking</a></li>
                                <li><a href="patient-dashboard.html"><i class="fas fa-angle-double-right"></i> Patient Dashboard</a></li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->

                    </div>

                    <div class="col-lg-3 col-md-6">

                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">For Doctors</h2>
                            <ul>
                                <li><a href="appointments.html"><i class="fas fa-angle-double-right"></i> Appointments</a></li>
                                <li><a href="chat.html"><i class="fas fa-angle-double-right"></i> Chat</a></li>
                                <li><a href="login.html"><i class="fas fa-angle-double-right"></i> Login</a></li>
                                <li><a href="doctor-register.html"><i class="fas fa-angle-double-right"></i> Register</a></li>
                                <li><a href="doctor-dashboard.html"><i class="fas fa-angle-double-right"></i> Doctor Dashboard</a></li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->

                    </div>

                    <div class="col-lg-3 col-md-6">

                        <!-- Footer Widget -->
                        <div class="footer-widget footer-contact">
                            <h2 class="footer-title">Contact Us</h2>
                            <div class="footer-contact-info">
                                <div class="footer-address">
                                    <span><i class="fas fa-map-marker-alt"></i></span>
                                    <p> 3556  Beech Street, San Francisco,<br> California, CA 94108 </p>
                                </div>
                                <p>
                                    <i class="fas fa-phone-alt"></i>
                                    +1 315 369 5943
                                </p>
                                <p class="mb-0">
                                    <i class="fas fa-envelope"></i>
                                    doccure@example.com
                                </p>
                            </div>
                        </div>
                        <!-- /Footer Widget -->

                    </div>

                </div>
            </div>
        </div>
        <!-- /Footer Top -->

        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="container-fluid">

                <!-- Copyright -->
                <div class="copyright">
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="copyright-text">
                                <p class="mb-0"><a href="templateshub.net">Templates Hub</a></p>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">

                            <!-- Copyright Menu -->
                            <div class="copyright-menu">
                                <ul class="policy-menu">
                                    <li><a href="term-condition.html">Terms and Conditions</a></li>
                                    <li><a href="privacy-policy.html">Policy</a></li>
                                </ul>
                            </div>
                            <!-- /Copyright Menu -->

                        </div>
                    </div>
                </div>
                <!-- /Copyright -->

            </div>
        </div>
        <!-- /Footer Bottom -->

    </footer>
    <!-- /Footer -->

</div>
<!-- /Main Wrapper -->
<!-- jQuery -->
<script src="{{asset('frontend/assets/js/jquery.min.js')}}"></script>

<!-- Bootstrap Core JS -->
<script src="{{asset('frontend/assets/js/popper.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/bootstrap.min.js')}}"></script>

<!-- Slick JS -->
<script src="{{asset('frontend/assets/js/slick.js')}}"></script>

<!-- Custom JS -->
<script src="{{asset('frontend/assets/js/script.js')}}"></script>

</body>

<!-- doccure/  30 Nov 2019 04:11:53 GMT -->
</html>
