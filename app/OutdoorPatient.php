<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutdoorPatient extends Model
{
    protected $fillable=[
                            'registration_no',
                            'name',
                            'age',
                            'phone',
                            'gender',
                            'patient_status',
                            'pregnant',
                            'peptic_ulcer',
                            'fatty_liver',
                            'sol_liver',
                            'acute_hepaties',
                            'obstructive_jaundice',
                            'liver_abscess',
                            'liver_cyst',
                            'comment',
                            'agree',
                        ];
}
