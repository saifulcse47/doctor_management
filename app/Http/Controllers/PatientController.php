<?php

namespace App\Http\Controllers;

use App\OutdoorPatient;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    public function registrationPatient(){
        return view('frontend.patient.add-patient');
    }
    public function SaveOutdoorPatient(Request $request){
//        dd($request->all());
        OutdoorPatient::create([
            'registration_no'=>$request->registration_no,
            'name'=>$request->name,
            'age'=>$request->age,
            'phone'=>$request->phone,
            'gender'=>$request->gender,
            'patient_status'=>$request->patient_status,
            'pregnant'=>$request->pregnant,
            'peptic_ulcer'=>$request->peptic_ulcer,
            'fatty_liver'=>$request->fatty_liver,
            'sol_liver'=>$request->sol_liver,
            'acute_hepaties'=>$request->acute_hepaties,
            'obstructive_jaundice'=>$request->obstructive_jaundice,
            'liver_abscess'=>$request->liver_abscess,
            'liver_cyst'=>$request->liver_cyst,
            'comment'=>$request->comment,
            'agree'=>$request->agree,

        ]);
        return  redirect()->back()->with('message','Save information  successfully');
    }
    public function UpdateOutdoorPatient(Request $request){
        $this->validate($request,[
            'registration_no'=>'required',
            'name'=>'required',
            'age'=>'required',
            'phone'=>'required',
            'age'=>'required',
            'agree'=>'required',
        ]);

            $patient=OutdoorPatient::find($request->id);

            $patient->registration_no          =$request->registration_no;
            $patient->name                     =$request->name;
            $patient->age                      =$request->age;
            $patient->phone                    =$request->phone;
            $patient->gender                   =$request->gender;
            $patient->patient_status           =$request->patient_status;
            $patient->pregnant                 =$request->pregnant;
            $patient->peptic_ulcer             =$request->peptic_ulcer;
            $patient->fatty_liver              =$request->fatty_liver;
            $patient->sol_liver                =$request->sol_liver;
            $patient->acute_hepaties           =$request->acute_hepaties;
            $patient->obstructive_jaundice     =$request->obstructive_jaundice;
            $patient->liver_abscess            =$request->liver_abscess;
            $patient->liver_cyst               =$request->liver_cyst;
            $patient->comment                  =$request->comment;
            $patient->agree                    =$request->agree;
            $patient->save();
//            dd($patient);

            return redirect('admin/manage/old/out-door/patient')->with('message','Update successfully');
    }
}
