<?php

namespace App\Http\Controllers;

use App\OutdoorPatient;
use Illuminate\Http\Request;
use Auth;

class AdminController extends Controller
{
    public function index(){
        $title = 'Admin Dashboard';
        return view('admin.dashboard', compact('title'));
    }
    public function AppointmentList(){
        return view('admin.home.appointment-list');
    }
    public function adminLogout(){
        Auth::guard('admin')->logout();
        return view('admin.login');
    }
    public function manageOldOutDoorPatient(){
        $patients=OutdoorPatient::all();
        return view('admin.patient.manage-patient',compact('patients'));
    }
    public function viewOutDoorPatient($id){
        $patient=OutdoorPatient::find($id);
        return view('admin.patient.view-outdoor-patient',compact('patient'));
    }
}
