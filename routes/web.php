<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//frontend route here
Route::get('/','indexController@index');
Route::get('/registration/patient','PatientController@registrationPatient')->name('registration.patient');
Route::post('/save/out-door/patient','PatientController@SaveOutdoorPatient')->name('save.out-door-patient');
//admin route start from here
Route::group(['prefix' => 'admin'], function (){
    Route::get('/sing-in','Admin\AdminLoginController@index')->name('login');
    Route::post('/login','Admin\AdminLoginController@adminLoginCheck')->name('admin.login');

    Route::middleware('auth:admin')->group(function (){
        Route::get('/dashboard', 'AdminController@index')->name('admin.dashboard');
        Route::get('/appointment/list', 'AdminController@AppointmentList')->name('appointment.list');
        Route::get('/logout', 'AdminController@adminLogout')->name('admin.logout');
        Route::get('/manage/old/out-door/patient', 'AdminController@manageOldOutDoorPatient')->name('manage.out-door.patient');
        Route::get('/view/patient/{id}', 'AdminController@viewOutDoorPatient')->name('view.patient');
        Route::post('/update/out-door/patient/','PatientController@UpdateOutdoorPatient')->name('update.out-door-patient');
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
